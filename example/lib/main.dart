import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:taximeter/taximeter.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  void dispose(){
    super.dispose();
  }



  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  void initServiceTrip() async {
    await Taximeter.initService((Trip trip){
        setState(() => _platformVersion = trip.toString());
        debugPrint(trip.toString());
        print(trip);
      }, 98763);

      final trip = await Taximeter.getTripByServiceId(98763);
      debugPrint("${trip == null}");
      print("object : ${trip.toString()}");
  }

  void finalizeService() async {
    await Taximeter.finilazeService;
    setState(() => _platformVersion = "");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(children: <Widget>[
          Center(
            child: Text('Running on: $_platformVersion\n'),
          ),
          TaximeterWidget(
            distance: "12133 M",
            monto: "\$2133",
            fontColor: Colors.red,
          ),
          MaterialButton(
            onPressed: initServiceTrip,
            child: Text("Init"),
          ),
          MaterialButton(
            onPressed: finalizeService,
            child: Text("Finalizar 0",
              style: TextStyle(
                fontSize: 22,
                package: "taximeter",
                fontFamily: "Orbitron"
              ),
            ),
          )
        ],),
      ),
    );
  }
}
