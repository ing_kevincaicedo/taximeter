package com.riotaximeter.test

import android.content.Context
import android.location.Location
import androidx.test.core.app.ApplicationProvider
import com.riotaximetre.taximeter.helps.TaximeterUtils
import org.junit.Test
import org.mockito.Mock
import java.time.Instant
import java.util.*


class LocationTest {

    val context = ApplicationProvider.getApplicationContext<Context>()

    @Mock
    val location: Location = Location("B")
    @Mock
    val location2: Location = Location("B")

    @Test
    fun location_velocity_validate(){

        location.latitude = 6.2578467
        location.longitude = -75.567496

        location2.latitude = 6.2569156
        location2.longitude = -75.5678466

        val distance = location.distanceTo(location2)

        val date = Date.from(Instant.ofEpochMilli(1573590153475))
        val isValidate = TaximeterUtils.validateVelocityTrip(date, distance = distance.toDouble())
        assert(isValidate)
    }

}