#import "TaximeterPlugin.h"
#import <taximeter/taximeter-Swift.h>

@implementation TaximeterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftTaximeterPlugin registerWithRegistrar:registrar];
}
@end
