export 'package:taximeter/trip.dart';
export 'package:taximeter/taximeter_widget.dart';

import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:taximeter/command_name.dart';
import 'package:taximeter/trip.dart';

class Taximeter {
  static const MethodChannel _channel =
      const MethodChannel('taximeter');

  static Future<bool> initService(final Function(Trip) onUpdate, int serviceId) async {
    final bool result = await _channel.invokeMethod(CommandName.INIT_SERVICE, serviceId);
    _channel.setMethodCallHandler((MethodCall methodCall) async {
      if(methodCall.method == CommandName.NEW_UPDATE_TRIP){
        final Trip trip = Trip.fromJson(jsonDecode(methodCall.arguments));
        onUpdate(trip);
      }
    });
    return result;
  }

  static Future<bool> get finilazeService async {
    return await _channel.invokeMethod(CommandName.FINISH_SERVICE);
  }

  static Future<Trip> getTripByServiceId(int serviceId) async {
    final tripString = await _channel.invokeMethod(CommandName.GET_TRIP_STATE, serviceId);
    Trip trip;
    try {
      trip = Trip.fromJson(jsonDecode(tripString));
    } catch (e) {}
    return trip;
  }
  
}