
import 'package:meta/meta.dart';

@immutable
class Trip {

  final int id;
  final DateTime date;
  final int sum;
  final double distance;
  final double lat;
  final double lon;

  const Trip({
    this.id = 0,
    this.date,
    this.distance = 0.0,
    this.lat = 0.0,
    this.lon = 0.0,
    this.sum = 0
  });

  Trip.fromJson(Map<String, dynamic> payload): 
    this.date = DateTime.fromMillisecondsSinceEpoch(payload["date"] ?? DateTime.now()),
    this.id = payload["id"],
    this.sum = payload["sum"],
    this.distance = payload["distance"],
    this.lat = payload["lat"],
    this.lon = payload["lon"];

  @override
  String toString() => """
    id: $id,
    date: $date,
    distance: $distance,
    lat: $lat,
    lon: $lon,
    sum: $sum
  """;
  
}
