import 'package:flutter/material.dart';

class TaximeterWidget extends StatelessWidget {
  final String distance;
  final String monto;

  final double fontSizeMonto;
  final double fontSizeDistance;

  final Color fontColor;

  TaximeterWidget(
      {this.distance,
      this.monto,
      this.fontSizeDistance = 14,
      this.fontSizeMonto = 24,
      this.fontColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(2),
            child: Text("$monto",
                style: TextStyle(
                    fontFamily: "Orbitron",
                    package: "taximeter",
                    fontWeight: FontWeight.w700,
                    fontSize: fontSizeMonto,
                    color:
                        fontColor ?? Theme.of(context).textTheme.title.color)),
          ),
          Container(
            padding: EdgeInsets.all(2),
            child: Text("$distance",
                style: TextStyle(
                    fontFamily: "Orbitron",
                    package: "taximeter",
                    fontSize: fontSizeDistance,
                    color:
                        fontColor ?? Theme.of(context).textTheme.title.color)),
          )
        ],
      ),
    );
  }
}
