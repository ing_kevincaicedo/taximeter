
abstract class CommandName {

  static const INIT_SERVICE = "init-service";
  static const FINISH_SERVICE = "finish-service";
  static const NEW_UPDATE_TRIP = "new-update-trip";
  static const GET_TRIP_STATE = "get-distance";
  
}