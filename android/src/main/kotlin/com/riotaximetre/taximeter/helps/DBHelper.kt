package com.riotaximetre.taximeter.helps

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.riotaximetre.taximeter.models.Trip
import java.util.*

class DBHelper(context: Context) : SQLiteOpenHelper(context, tableName, null, dbVersion) {

    /**
     * Sentence SQL Create State
     */
    private val SQL_CREATE_TABLE = "CREATE TABLE $tableName(id INTEGER PRIMARY KEY, date TEXT, sum INTEGER, distance REAL, lat REAL, lon REAL);"
    private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $tableName"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    fun saveStateTrip(trip: Trip): Boolean {
        // serviceId: Int, date: Long, sum: Double, distance: Double, isrun: Int, lat: Double, lon: Double
        val cv = ContentValues()
        cv.put("id", trip.id)
        cv.put("date", trip.date?.time.toString())
        cv.put("sum", trip.sum)
        cv.put("distance", trip.distance)
        cv.put("lat", trip.lat)
        cv.put("lon", trip.lon)

        try {
            deleteTripAll()
            this.writableDatabase.insert(tableName, null, cv)
        } catch (e: Exception) {
            return false
        }

        return true
    }

    fun updateStateTrip(trip: Trip): Boolean {
        val cv = ContentValues()
        cv.put("date", trip.date?.time.toString())
        cv.put("sum", trip.sum)
        cv.put("distance", trip.distance)
        cv.put("lat", trip.lat)
        cv.put("lon", trip.lon)

        try {
            this.writableDatabase.update(tableName, cv, "id=" + trip.id, null)
        } catch (e: Exception) {
            return false
        }

        return true
    }

    fun readStateTrip(id: Int): Trip? {
        var trip: Trip? = null
        val selectQuery = "SELECT * FROM $tableName WHERE id=$id"
        val cursor = this.writableDatabase.rawQuery(selectQuery, null)
        try {
            if (cursor.moveToFirst()) {
                do {
                    val idr = cursor.getInt(0)
                    val date = Date(java.lang.Long.parseLong(cursor.getString(1)))
                    val sum = cursor.getInt(2)
                    val distance = cursor.getDouble(3)
                    val lat = cursor.getDouble(4)
                    val lon = cursor.getDouble(5)
                    trip = Trip(idr, date, sum, distance, lat, lon)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            cursor.close()
        }

        return trip
    }

    fun deleteTripById(id: Int): Boolean {
        var isDelete = false
        try {
            val deleted = this.writableDatabase.delete(tableName, "id=$id", null).toLong()
            isDelete = (deleted > 0)
        } catch (ex: Exception) {
            Log.e("DBHelper", "deleteOfferById:" + ex.message)
        }

        return isDelete
    }

    fun deleteTripAll(): Boolean {
        var isDelete = false
        try {
            val deleted = this.writableDatabase.delete(tableName, null, null).toLong()
            isDelete = deleted > 0
        } catch (ex: Exception) {
            Log.e("DBHelper", "deleteOfferById:" + ex.message)
        }

        return isDelete
    }

    companion object {

        const val tableName = "history_taximeter"
        const val dbVersion = 1

        private var dbHelper: DBHelper? = null

        @JvmStatic
        fun getInstance(context: Context): DBHelper? {
            if (dbHelper != null) return dbHelper

            dbHelper = DBHelper(context)
            return dbHelper
        }
    }

}