package com.riotaximetre.taximeter.helps

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

object TaximeterUtils {

    fun calculatePrice(distance: Double, prefTarifEnter: Double, prefTarif: Double): Double {
        return if (distance < Keys.distanceOfCost) prefTarifEnter else prefTarif * (distance / Keys.distanceOfCost).toInt() + prefTarifEnter
    }

    fun validateVelocityTrip(lastDate: Date, distance: Double): Boolean {

        val date = Calendar.getInstance().time
        val diff = abs(date.time - lastDate.time)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(diff).toDouble()
        var secons = TimeUnit.MILLISECONDS.toSeconds(diff).toDouble()
        val milisecons = TimeUnit.MILLISECONDS.toMillis(diff).toDouble()
        var time = minutes / 60.0

        if (secons <= 0)
            secons = milisecons / 1000.0

        if (minutes <= 0)
            time = secons / 3600.0

        val speed = distance / 1000.0 / time
        return speed < Keys.maxSpeed
    }

    fun isServiceRunning(serviceClass: Class<*>, activity: Activity): Boolean {
        val manager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}