package com.riotaximetre.taximeter.helps

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class LocationUtils {

    companion object {

        @JvmStatic
        fun isGrantedLocationPermission(context: Context): Boolean {
            return !(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        }

        @JvmStatic
        fun isGPSLocationAvailable(context: Context): Boolean {
            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

        @JvmStatic
        fun isGooglePlayAvailable(context: Context): Boolean {
            return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS
        }


        /**
         * Location
         */
        @JvmStatic
        fun getCurrentLocation(context: Context, onResult: ((location: Location?) -> Unit)) {
            if (isGooglePlayAvailable(context))
                getLastLocationByGoogleService(context, onResult)
            else getLastLocation(context, onResult)
        }

        @JvmStatic
        private fun getLastLocation(context: Context, callbacks: ((location: Location?) -> Unit)) {
            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            callbacks(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER))
        }

        @JvmStatic
        private fun getLastLocationByGoogleService(context: Context, callbacks: ((location: Location?) -> Unit)) {
            val mFusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            mFusedLocationClient.lastLocation?.addOnSuccessListener { location: Location? ->
                callbacks(location)
            }
        }

    }
}