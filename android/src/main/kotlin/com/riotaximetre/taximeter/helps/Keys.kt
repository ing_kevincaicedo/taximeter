package com.riotaximetre.taximeter.helps

object Keys {

    const val tableName: String = "riotaximetre"
    const val dbVersion: Int = 1

    const val distanceOfCost = 78
    const val maxSpeed = 70

    const val PARAM_INTENT = "pendingIntent"

    const val UPDATE_TIME = 1000 // 1000 en Milisegundos
    const val DISTANCE_FOR_UPDATE = 10 // 10 en Metros

    const val WAIT_TIME = 60 // 60 en Segundos
    const val TEST_DELAY = 4800L // Tiempo espera actualizacion gps
    const val DELAY_TIME_ML = 1005L

    /**
     * The name of the channel for notifications.
     */
    const val CHANNEL_ID = "channel_01"

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    const val NOTIFICATION_ID = 12345678
    const val COMPANY_NAME = "Taximetro"

    const val PARAM_UPDATE_TRIP = "trip_taximeter"
    const val PARAM_CODE_UPDATE_TRIP = 2249
}