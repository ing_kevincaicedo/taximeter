package com.riotaximetre.taximeter.helps

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import com.riotaximetre.taximeter.R

class MNotification {

    companion object {

        @JvmStatic
        fun createNotification(context: Context, text: String, ongoing: Boolean = true, channelId: Int = Keys.NOTIFICATION_ID ): Notification {
            val intent = Intent(context, context::class.java)
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val builder = NotificationCompat.Builder(context, Keys.CHANNEL_ID)
            val icon = BitmapFactory.decodeResource(context.resources, R.drawable.taximeter)

            builder.setSmallIcon(android.R.drawable.ic_media_play)
                    .setContentTitle(Keys.COMPANY_NAME)
                    .setContentText(text)
                    .setOngoing(ongoing)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                    .setSmallIcon(R.drawable.taximeter)
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    //.setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(pendingIntent)
                    .setTicker(text)

            val notification = builder.build()
            // notification.flags = Notification.FLAG_ONGOING_EVENT;
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val mChannel = NotificationChannel(Keys.CHANNEL_ID, Keys.COMPANY_NAME, NotificationManager.IMPORTANCE_HIGH)
                if( notificationManager.getNotificationChannel(Keys.CHANNEL_ID) == null)
                    notificationManager.createNotificationChannel(mChannel)
            }
            notificationManager.notify(channelId, notification)

            return notification
        }
    }

}