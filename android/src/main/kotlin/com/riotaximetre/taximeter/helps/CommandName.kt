package com.riotaximetre.taximeter.helps

enum class CommandName constructor(private val `val`: String) {
    INIT_SERVICE( "init-service"),
    FINISH_SERVICE( "finish-service"),
    GET_TRIP_STATE("get-distance"),
    NEW_UPDATE_TRIP("new-update-trip");

    override fun toString(): String {
        return this.`val`
    }
}