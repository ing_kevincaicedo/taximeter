package com.riotaximetre.taximeter.helps

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper

object JSONObjectMapper {

    var objectMapper: ObjectMapper = ObjectMapper()

    private val instance: ObjectMapper get() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return objectMapper
    }

    fun serialize(message: Any): String? {
        var serializedMessage: String? = null
        try {
            serializedMessage = instance.writeValueAsString(message)
        } catch (e: JsonProcessingException) {

        }

        return serializedMessage
    }

}