package com.riotaximetre.taximeter.models

import java.util.*


class Trip {

    var id: Int = 0
    var date: Date? = null
    var sum: Int = 0
    var distance: Double = 0.0
    var lat: Double = 0.0
    var lon: Double = 0.0

    constructor(id: Int, date: Date, sum: Int, distance: Double) {
        this.id = id
        this.date = date
        this.sum = sum
        this.distance = distance
    }

    constructor(id: Int, date: Date, sum: Int, distance: Double, lat: Double, lon: Double) {
        this.id = id
        this.date = date
        this.sum = sum
        this.distance = distance
        this.lat = lat
        this.lon = lon
    }
}
