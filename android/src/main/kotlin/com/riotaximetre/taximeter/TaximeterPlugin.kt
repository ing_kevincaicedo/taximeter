package com.riotaximetre.taximeter

import com.riotaximetre.taximeter.helps.CommandName.GET_TRIP_STATE
import com.riotaximetre.taximeter.helps.CommandName.FINISH_SERVICE
import com.riotaximetre.taximeter.helps.CommandName.INIT_SERVICE
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

class TaximeterPlugin(private val delegate: TaximeterPluginDelegate): MethodCallHandler {

  companion object {

    @JvmStatic
    fun registerWith(registrar: Registrar) {

      val channel = MethodChannel(registrar.messenger(), "taximeter")
      val delegate = TaximeterPluginDelegate(registrar.activity(), channel)
      registrar.addActivityResultListener(delegate)
      channel.setMethodCallHandler(TaximeterPlugin(delegate))
    }
  }

  override fun onMethodCall(call: MethodCall, result: Result) {

    when(call.method){
      INIT_SERVICE.toString() -> delegate.initService(call, result)
      FINISH_SERVICE.toString() -> delegate.finalizeService(result)
      GET_TRIP_STATE.toString() -> delegate.getTripByServiceId(call, result)
      else -> result.notImplemented()
    }
  }
}
