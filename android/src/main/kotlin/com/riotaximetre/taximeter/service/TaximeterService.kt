package com.riotaximetre.taximeter.service

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import com.google.android.gms.location.*
import com.riotaximetre.taximeter.helps.*
import com.riotaximetre.taximeter.helps.Keys.DELAY_TIME_ML
import com.riotaximetre.taximeter.helps.Keys.TEST_DELAY
import com.riotaximetre.taximeter.helps.Keys.WAIT_TIME
import com.riotaximetre.taximeter.models.Trip
import java.util.*

class TaximeterService: Service(), LocationListener {

    private val binder = LocalBinder()

    private var pendingIntent: PendingIntent? = null

    /**
     * Manager Location request update for devices no available Google Play Service
     */
    private var locationManager: LocationManager? = null

    /**
     * Provides access to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Callback for changes in location.
     */
    private var mLocationCallback: LocationCallback? = null

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000

    /**
     * The minimum displacement between location updates in meters
     */
    private val LOCATION_UPDATE_SMALLEST_DISPLACEMENT_METERS: Float = 2f

    /**
     * Taximeter state memory storage object
     */
    private var trip: Trip? = null

    private var dbhelper: DBHelper? = null

    /**
     * Timer for time waiting
     */
    private val handler: Handler by lazy { Handler() }

    private val runnable: Runnable by lazy { Runnable { onTimeUpdate() } }

    private var cronoTime = 0

    private var hasMadeLocationUpdate = false

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

    override fun onLocationChanged(location: Location?) {
        hasMadeLocationUpdate = true
        if(location != null) updateTripWith(location)
    }

    override fun onDestroy() {
        mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
        dbhelper?.close()
        dbhelper = null
        handler.removeCallbacks(runnable)
        locationManager?.removeUpdates(this)
        stopForeground(true)
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        startForeground(Keys.NOTIFICATION_ID, MNotification.createNotification(this, "Taximetro"))
        pendingIntent = intent?.getParcelableExtra(Keys.PARAM_INTENT)
        LocationUtils.getCurrentLocation(this, onResult = {})
        return binder
    }

    inner class LocalBinder : Binder() {
        val service
            get() = this@TaximeterService
    }


    fun initTrip(serviceId: Int){
        try {
            val mTrip = readTripState(serviceId)
            if(mTrip != null) {
                trip = mTrip
                sendMessageNotify()
                LocationUtils.getCurrentLocation(this, onResult = { location ->
                    if(location != null) updateTripWith(location)
                })
            } else {
                LocationUtils.getCurrentLocation(this, onResult = { location ->
                    if(location != null) setInitialTripWith(location, serviceId)
                })
            }
        } catch (e: Exception){
            println("${e.message}")
        }

        initLocationRequest()
        handler.postDelayed(runnable, TEST_DELAY)
    }

    fun finalizeTrip(){
        deleteTripState(trip!!)
    }

    private fun setInitialTripWith(location: Location, serviceId: Int){
        trip = Trip(id = serviceId, distance = 0.0, date = Date(), sum = 0)
        trip?.lat = location.latitude
        trip?.lon = location.longitude
        if(trip != null) writeTripState(trip = trip!!)
    }

    private fun updateTripWith(sumTime: Int){
        trip?.sum = trip?.sum!! + sumTime
        updateTripState(trip!!)
        sendMessageNotify()
    }

    private fun onTimeUpdate(){
        if (hasMadeLocationUpdate) {
            handler.postDelayed(runnable, TEST_DELAY)
            hasMadeLocationUpdate = false
            return
        } else {
            handler.postDelayed(runnable, DELAY_TIME_ML)
        }

        cronoTime += 1
        if (cronoTime >= WAIT_TIME) {
            updateTripWith(cronoTime)
            cronoTime = 0
        }
    }

    private fun updateTripWith(location: Location){
        try {
            val lastLocation = Location("A")
            lastLocation.latitude = trip?.lat!!
            lastLocation.longitude = trip?.lon!!

            if(lastLocation.latitude == 0.0 || lastLocation.longitude == 0.0)
                return

            val distance: Double = lastLocation.distanceTo(location).toDouble()
            if(!TaximeterUtils.validateVelocityTrip(lastDate = trip?.date!!, distance = distance))
                return

            trip?.lat = location.latitude
            trip?.lon = location.longitude
            trip?.distance = trip?.distance!! + distance
            trip?.date = Date()
            updateTripState(trip!!)
            sendMessageNotify()
        } catch (e: Exception){
            println("${e.message}")
        }
    }


    /**
     * Update info ui
     *
     */
    private fun sendMessageNotify(){
        val intent = Intent()
        intent.putExtra(Keys.PARAM_UPDATE_TRIP, JSONObjectMapper.serialize(trip!!))

        try {
            pendingIntent?.send(this@TaximeterService, Keys.PARAM_CODE_UPDATE_TRIP, intent)
        } catch (e: PendingIntent.CanceledException) {
            e.printStackTrace()
            println("${e.message}")
        }
    }


    /**
    * Management store state taximeter trip
     *
     */
    private fun readTripState(id: Int): Trip? {
        if(dbhelper == null)
            dbhelper = DBHelper.getInstance(this)

        return dbhelper?.readStateTrip(id = id)
    }

    private fun writeTripState(trip: Trip): Boolean? {
        if(dbhelper == null)
            dbhelper = DBHelper.getInstance(this)

        return dbhelper?.saveStateTrip(trip)
    }

    private fun updateTripState(trip: Trip): Boolean? {
        if(dbhelper == null)
            dbhelper = DBHelper.getInstance(this)

        return dbhelper?.updateStateTrip(trip)
    }

    private fun deleteTripState(trip: Trip): Boolean? {
        if(dbhelper == null)
            dbhelper = DBHelper.getInstance(this)

        dbhelper?.deleteTripById(trip.id)
        return dbhelper?.deleteTripAll()
    }


    /**
     * Listen for update location
     *
     * This support google play service and support old device with support to google play service
     * Recive update location for google play service in LocationCallback.onLocationResult object
     * for old api location manager recive in this.onLocationChanged
     */
    private fun initLocationRequest(){
        if (LocationUtils.isGooglePlayAvailable(applicationContext))
            return startLocationUpdates()

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ( LocationUtils.isGrantedLocationPermission(this) )
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS,
                    LOCATION_UPDATE_SMALLEST_DISPLACEMENT_METERS, this@TaximeterService)
    }

    private fun startLocationUpdates() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)

        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = UPDATE_INTERVAL_IN_MILLISECONDS / 2
        mLocationRequest.smallestDisplacement = LOCATION_UPDATE_SMALLEST_DISPLACEMENT_METERS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if(locationResult?.lastLocation != null) {
                    hasMadeLocationUpdate = true
                    updateTripWith(locationResult.lastLocation)
                }
            }
        }

        if( LocationUtils.isGrantedLocationPermission(this) )
            mFusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }

}