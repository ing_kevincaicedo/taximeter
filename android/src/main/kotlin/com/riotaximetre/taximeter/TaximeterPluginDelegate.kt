package com.riotaximetre.taximeter

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.widget.Toast
import com.riotaximetre.taximeter.helps.CommandName.NEW_UPDATE_TRIP
import com.riotaximetre.taximeter.helps.DBHelper
import com.riotaximetre.taximeter.helps.JSONObjectMapper
import com.riotaximetre.taximeter.helps.Keys
import com.riotaximetre.taximeter.helps.LocationUtils
import com.riotaximetre.taximeter.service.TaximeterService
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.PluginRegistry

class TaximeterPluginDelegate(private val activity: Activity, private val channel: MethodChannel): PluginRegistry.ActivityResultListener {

    private var counterService: TaximeterService? = null

    private var isServiceConnected = false

    private var serviceId: Int? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if(requestCode == Keys.PARAM_CODE_UPDATE_TRIP){
            val tripString = data?.getStringExtra(Keys.PARAM_UPDATE_TRIP)
            channel.invokeMethod(NEW_UPDATE_TRIP.toString(), tripString)
            return true
        }

        return false
    }

    fun initService(methodCall: MethodCall, result: MethodChannel.Result) {
        LocationUtils.getCurrentLocation(activity, onResult = {})
        val pendingIntent = activity.createPendingResult(Keys.PARAM_CODE_UPDATE_TRIP, Intent(), 0)
        val intent = Intent(activity, TaximeterService::class.java).putExtra(Keys.PARAM_INTENT, pendingIntent)
        serviceId = methodCall.arguments as Int
        activity.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        result.success(true)
    }

    fun getTripByServiceId(methodCall: MethodCall, result: MethodChannel.Result){
        val serviceId = methodCall.arguments as Int
        val trip = DBHelper.getInstance(activity)?.readStateTrip(id = serviceId)
        if(trip != null)
            result.success( JSONObjectMapper.serialize(trip) )
        else
            result.success(null)
    }

    fun finalizeService(result: MethodChannel.Result){
        counterService?.finalizeTrip()
        val intent = Intent(activity, TaximeterService::class.java)
        activity.stopService(intent)
        activity.unbindService(connection)
        result.success(true)
    }

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder = iBinder as TaximeterService.LocalBinder
            counterService = binder.service
            counterService?.initTrip(serviceId!!)
            isServiceConnected = true
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            isServiceConnected = false
            Toast.makeText(activity, "Servicio desconectado", Toast.LENGTH_LONG).show()
        }
    }

}